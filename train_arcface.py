from io import RawIOBase
import os
import math
import numpy as np
import torch
from datetime import datetime

from torch.utils.data import DataLoader
from torch.nn.utils import clip_grad_norm_
from torch.optim import Adam, SGD
from torch.optim.lr_scheduler import CosineAnnealingLR, StepLR
from tqdm import tqdm
from tensorboardX import SummaryWriter

import sys
sys.path.append(os.getcwd())

import utils
from dataset import ImageFolder
from config import Config
from backbone.resnet import ResNet_50, ResNet_101, ResNet_152
from backbone.model_irse import IR_SE_50, IR_SE_101, IR_SE_152
from backbone.iresnet import iresnet50, iresnet100
from head import metric
from loss import focal_loss

writer = SummaryWriter('runs/run_{}'.format(datetime.now().strftime('%H:%M:%S_%d%m%Y')))

train_data = ImageFolder(Config.dataroot, phase='train')
trainloader = DataLoader(train_data, batch_size=Config.batch_size, 
                         shuffle= True, num_workers= Config.num_workers,
                         drop_last= True)

test_data = ImageFolder(Config.testroot, phase='test')
testloader = DataLoader(test_data, batch_size=1)

if Config.backbone == 'resnet_50':
    model = ResNet_50(Config.image_size)
elif Config.backbone == 'resnet_101':
    model = ResNet_101(Config.image_size)
elif Config.backbone == 'resnet_152':
    model = ResNet_152(Config.image_size)
elif Config.backbone == 'irse_50':
    model = IR_SE_50(Config.image_size)
elif Config.backbone == 'irse_101':
    model = IR_SE_101(Config.image_size)
elif Config.backbone == 'irse_152':
    model = IR_SE_152(Config.image_size)
elif Config.backbone == 'ir100':
    model = iresnet100()
elif Config.backbone == 'ir50':
    model = iresnet50()
else:    
    raise NotImplementedError

if Config.pretrained_backbone:
    model.load_state_dict(torch.load(Config.pretrained_backbone, map_location= Config.device))
    
if Config.metric == 'arcface':
    metric_fc = metric.ArcFace(512, Config.num_class)
elif Config.metric == 'sphereface':
    metric_fc = metric.SphereFace(512, Config.num_class)
else:
    raise NotImplementedError

criterion = focal_loss.FocalLoss(gamma=2)

if Config.optimizer == 'sgd':
    optimizer_backbone = SGD([{'params': model.parameters()}, {'params': metric_fc.parameters()}], 
                    lr= Config.lr, weight_decay= Config.weight_decay)
    optimizer_head = SGD(params=metric_fc.parameters(), lr= Config.lr, weight_decay= Config.weight_decay)
else: 
    optimizer = Adam([{'params': model.parameters()}, {'params': metric_fc.parameters()}], 
                    lr= Config.lr, betas=(0.95, 0.99), weight_decay=Config.weight_decay)

schedule_lr = StepLR(optimizer, step_size=Config.lr_step, gamma=0.1)

def train():
    model.train().to(Config.device)
    metric_fc.train().to(Config.device)
    for e in range(Config.epoch):
        pbar = tqdm(iterable= iter(trainloader), total= len(trainloader))
        losses = utils.AverageMeter()
        top1 = utils.AverageMeter()
        top5 = utils.AverageMeter()

        for i, data in enumerate(pbar):
            imgs, labels = data
            imgs = imgs.to(Config.device)
            labels = labels.to(Config.device).long()
            features = model(imgs)
            outputs = metric_fc(features, labels)
            
            loss, _ = criterion(outputs, labels)
            prec1, prec5 = utils.accuracy(outputs.data, labels, topk = (1,5))
            losses.update(loss.data.item(), imgs.size(0))
            top1.update(prec1.data.item(), imgs.size(0))
            top5.update(prec5.data.item(), imgs.size(0))

            optimizer.zero_grad()
            loss.backward()
            utils.grad_clipping(optimizer, Config.grad_clip)
            optimizer.step()
            pbar.set_description('Epoch %d/%d  Loss %.4f' % (e, Config.epoch, loss))
            writer.add_scalar('Train Loss', loss, e * len(trainloader) + i)

        epoch_loss = losses.avg
        epoch_acc = top1.avg
        writer.add_scalar('Training Loss', epoch_loss, e+1)
        writer.add_scalar('Training Acc', epoch_acc, e+1)
        print(' Training_Loss {loss.val:.4f} ({loss.avg:.4f}) \
                Training_prec@1 {top1.val:.4f} ({top1.avg:.4f}) \
                Training_prec@5 {top5.val:.4f} ({top5.avg:.4f})'.format(
                loss=losses, top1=top1, top5=top5
            ))
        schedule_lr.step()
        torch.save(model.state_dict(), Config.save_model_dir + '/epoch_{}_top1_{}_acc{:.4f}.pth'.format(e, Config.backbone, top1.avg)) 


if __name__ == "__main__":
    train()

# Embedding 512 D
# Arcface
