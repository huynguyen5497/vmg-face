import numpy as np
import torch
import os 
import random
import torchvision.transforms as transforms
from torch.utils.data import Dataset, DataLoader
from PIL import Image
import imgaug as ia
import imgaug.augmenters as iaa
from sklearn.model_selection import KFold


class ImageFolder(Dataset):
    def __init__(self, dataroot, phase='train'):
        if phase == 'train':
            self.trans = transforms.Compose([
                transforms.Resize((112,112)),
                transforms.RandomHorizontalFlip(),
                # transforms.ColorJitter(brightness=0.125, contrast=0.125, saturation=0.125),             
                transforms.ToTensor(),
                transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
            ])
        elif phase == 'test':
            self.trans = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
            ])
        else:
            raise NotImplementedError

        self.images = []
        id_labels = os.listdir(dataroot)
        for id_label in id_labels:
            folder_path = os.path.join(dataroot, id_label)
            for img in os.listdir(folder_path):
                # if 'sf' in img or 'origin' in img:
                self.images.append(os.path.join(folder_path, img))
        random.shuffle(self.images)
        
        self.l2i = {v:i for i,v in enumerate(id_labels)}

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        img = Image.open(self.images[idx])
        img = self.trans(img)
        label = self.images[idx].split('/')[1]
        label = self.l2i[label]
        label = torch.tensor(label)
        return img, label


class TwoImgDataset(object):
    def __init__(self, root):
        self.root = root
        self.idx = os.listdir(root)

    def gen_batch(self, batch_size, img_shape):
        h,w,c = img_shape
        pair_batch = [np.zeros((batch_size, c, h, w)) for _ in range(2)]
        for i in range(batch_size):
            if random.random() < 0.5:
                imgs, label = self.get_pair_same_image()
            else:
                imgs, label = self.get_pair_diff_image()
            img1 = Image.open(imgs[1])
            img2 = Image.open(imgs[2])
            pair_batch[0][i] = np.array(img1).transpose(2,0,1)
            pair_batch[1][i] = np.array(img2).transpose(2,0,1)

        return pair_batch, label 

    def get_pair_same_image(self):
        for idx in os.listdir(self.root):
            idx_folder = os.path.join(self.root, idx)
            images = os.listdir(idx_folder)
            if len(images) >= 2:
                for i in range(len(images)-1):
                    for j in range(i+1, len(images)):
                        img1_path = os.path.join(idx_folder, images[i])
                        img2_path = os.path.join(idx_folder, images[j])                   
                        yield ([img1_path, img2_path], 1)   

    def get_pair_diff_image(self):
        for idx in os.listdir(self.root):
            idx_folder = os.path.join(self.root, idx)
            images = os.listdir(idx_folder)
            
            yield


if __name__ == "__main__":
    from tqdm import tqdm
    mean = (0.485, 0.456, 0.406)
    std = (0.229, 0.224, 0.225)
    path = 'img_mtcnn'
    data = ImageFolder(path)
    trainloader = DataLoader(data, batch_size=32)
    for _ in range(1):
        for imgs, labels in tqdm(iter(trainloader)):
            for i in range(imgs.size(0)):
                img = imgs[i].mul_(std).add_(mean)
                img *= 255
                img = img.permute(1,2,0)
                img = np.array(img)
                print(img)
                # print(label)