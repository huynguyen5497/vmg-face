import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.dropout import Dropout

class DuAn(nn.Module):
    def __init__(self,nc_inp, num_class, dropout=0.25):
        super(DuAn).__init__()
        self.num_class = num_class
        self.network = nn.Sequential([
            nn.Linear(nc_inp, num_class),
            nn.Relu(),
            nn.Dropout(dropout),
            nn.Linear(num_class, num_class),
            nn.ReLU(),
        ])
    
    def forward(self, x):
        x = self.network(x)
        return x

