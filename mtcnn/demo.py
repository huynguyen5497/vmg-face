import cv2 
import numpy as np
from PIL import Image
import math
import sys

from numpy.lib.utils import byte_bounds
sys.path.append('arcface')

from detector import detect_faces
from visualization_utils import show_bboxes

import os

from utils import get_central_face_attributes, align_face


if __name__ == '__main__':
    # img = Image.open('/home/kid/Documents/huynd/Face/DataSet_Align/84325082555/84325082555_sf_01.png')
    # bounding_boxes, landmarks = detect_faces(img)
    # # bb = np.ceil(bounding_boxes[0][:4])
    # # bounding_boxes[0][:4] = np.ceil(bounding_boxes[0][:4])
    # # print(bounding_boxes)
    # img = cv.cvtColor(np.array(img), cv.COLOR_RGB2BGR)
    # show_bboxes(img, bounding_boxes, landmarks)

#--- Single image
    # filename = '/home/kid/Documents/huynd/Face/DataSet_Align/84325082555/84325082555_sf_01.png'

    # bounding_boxes, landmarks = get_central_face_attributes(filename)
    # img = align_face(filename, landmarks)
    # cv2.imwrite('2.jpg', img)

#--- Process all image
    # root = 'data_train_10k'
    # root_save = 'data_train_align'
    # if not os.path.exists(root_save):
    #     os.mkdir(root_save)

    # for folder in os.listdir(root):
    #     for img_name in os.listdir(os.path.join(root, folder)):
    #         img_path = os.path.join(root, folder, img_name)
    #         # img = Image.open(img_path)
    #         try:
    #             bboxes, landmarks = get_central_face_attributes(img_path)
    #             # for i, bb in enumerate(bboxes):
    #             #     # x1,y1,x2,y2 = np.ceil(bb[:4])
    #             #     x1,y1,x2,y2 = bb
    #             #     crop = np.array(img)[int(y1):int(y2), int(x1):int(x2), :]   
    #             #     crop_face = Image.fromarray(crop)
                
    #             img_res = align_face(img_path, landmarks)
    #             if not os.path.exists(os.path.join(root_save, folder)):
    #                 os.mkdir(os.path.join(root_save, folder))
    #             # new_name = img_name.split('.')[0] + '_crop' + str(i) +'.png' 
    #             cv2.imwrite(os.path.join(root_save, folder, img_name), img_res)
    #         except Exception as e:
    #             print(e)
    #             continue

# Crop face
    root = 'img_mt_enhance'
    dst = 'mt_crop'
    if not os.path.exists(dst):
        os.mkdir(dst)

    for idx in os.listdir(root):
        folder_path = os.path.join(root, idx)
        if os.listdir(folder_path) != []:
            for img_name in os.listdir(folder_path):
                img_path = os.path.join(folder_path, img_name)
                img = Image.open(img_path)
                try:
                    bbox, lmk = get_central_face_attributes(img)
                    x1,y1,x2,y2 = bbox[:4]
                    crop = np.array(img)[int(y1):int(y2), int(x1):int(x2), :]
                    crop = Image.fromarray(crop)
                    if not os.path.exists(os.path.join(dst, idx)):
                        os.mkdir(os.path.join(dst, idx))
                    # img_name = '84' + img_name[1:]
                    crop.save(os.path.join(dst, idx, img_name))
                except Exception as e:
                    print(img_path)
                    print(e)
                    continue
        else:
            continue