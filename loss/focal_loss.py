import torch
import torch.nn as nn

class FocalLoss(nn.Module):
    def __init__(self, gamma=2, epsilon= 1e-7):
        super(FocalLoss, self).__init__()
        self.ce = nn.CrossEntropyLoss()
        self.gamma = gamma
        self.epsilon = epsilon

    def forward(self, pred, target):
        logp = self.ce(pred, target)
        p = torch.exp(-logp)
        loss = (1 - p) ** self.gamma * logp
        return loss.mean(), logp

    