import torch
import torch.nn as nn
import torch.nn.functional as F 
from backbone import resnet


class SiameseNetwork(nn.Module):
    def __init__(self, size, pretrained=False) -> None:
        super(SiameseNetwork).__init__()
        # self.cnn = resnet.ResNet_50(size)
        self.cnn = resnet.ResNet_152(size)
        if pretrained:
            self.cnn.load_state_dict(torch.load(pretrained))
        self.linear = nn.Linear(512, 1)
        self.sig = nn.Sigmoid()
    
    def forward(self, inputs):
        feature_1 = self.cnn(inputs[0]) # [1, 512]
        feature_2 = self.cnn(inputs[1])
        dist = torch.abs(torch.sub(feature_1, feature_2))
        output = self.linear(dist)
        output = self.sig(output)
        
        return output