from math import pi
import pickle
from time import clock_settime
from faiss.swigfaiss import range_search_inner_product
import matplotlib
from numpy.lib.function_base import append
from pandas.core.arrays import boolean
from sklearn import preprocessing
from sklearn.metrics.classification import _weighted_sum
import torch
import numpy as np
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
import os
import argparse
import joblib
import pandas as pd
import cv2
import faiss
import imgaug as ia
import imgaug.augmenters as  iaa
from tqdm import tqdm
from scipy.spatial import distance
from mtcnn import detector
from backbone import resnet
from backbone.model_irse import IR_50, IR_SE_50, IR_SE_152
from backbone.inception_resnet_v1 import InceptionResnetV1
from backbone.iresnet import iresnet100, iresnet50
import align_face
from PIL import Image
from sklearn.svm import SVC, LinearSVC
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import precision_score, recall_score, classification_report, confusion_matrix
from sklearn.preprocessing import LabelEncoder

def augment(img):
    seq = iaa.Sequential([
    iaa.OneOf([
        iaa.Fliplr(0.5),
        iaa.AddToHueAndSaturation((-20,20)),
        iaa.GaussianBlur((0,2.0)),
        iaa.Sharpen(alpha=(0,0.5), lightness=(0.7,1.3)),
        iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5),
        iaa.pillike.EnhanceSharpness(3.0),
        iaa.pillike.EnhanceColor(2.0),
        iaa.Multiply((0.5, 1.5))
        ])
    ])
    return seq(image=img)

def prewhiten(img):
    mean = img.mean()
    std = img.std()
    std_adj = std.clamp(min=1.0/(float(img.numel()) ** 0.5))
    out = (img - mean) / std_adj
    return out

def image_standard(img):
    return (img - 127.5) / 128

def load_data(emb_path, label_path):
    all_embs = np.load(emb_path, allow_pickle= True)
    all_label = np.load(label_path)
    return all_embs, all_label

# def plot_distance(embs, labels):
#     match = []
#     unmatch = []

#     for i in range(len(labels)-1):
#         for j in range(i+1, len(labels)):
#             if labels[i] == labels[j]:
#                 match.append(distance.euclidean(embs[i], embs[j]))
#             else:
#                 unmatch.append(distance.euclidean(embs[i], embs[j]))

#     _,_,_ = plt.hist(match, bins=100)
#     _,_,_ = plt.hist(unmatch, bins=100, fc=(1,0,0,0.5))
#     # plt.show()
#     plt.savefig('distance_unmatch.png')

def classify(data, labels, debug=True):
    
    le = LabelEncoder()
    le.fit(labels)
    target_names = list(le.classes_)
    l2i = le.transform(labels)

    X_train, X_test, y_train, y_test = train_test_split(data, l2i, test_size=0.33, random_state=0)

    Cs = [1., 10., 15., 20., 100., 1000.]
    parameters = {'C':Cs}
    # svm = LinearSVC(multi_class='ovr')
    svm = SVC(kernel='linear', probability= True)
    clf = GridSearchCV(svm, parameters, cv=2)
    clf.fit(X=X_train, y=y_train)
    with open('model/model_svm.pkl', 'wb') as f:
        joblib.dump(clf, f)

    if debug:
        print('Best GridSearch', clf.best_estimator_)
        y_pred = clf.predict(X_test)
        print(confusion_matrix(y_test, y_pred, labels=range(len(target_names))))
        print(classification_report(y_test, y_pred, target_names= target_names))

def save_embs(all_embs, all_labels):
    with open(os.path.join(args.save_dir, 'all_embs_train.npy'), 'wb') as f1:
        np.save(f1, all_embs)
    with open(os.path.join(args.save_dir, 'all_label_train.npy'), 'wb') as f2:
        np.save(f2, all_labels)

def save_faiss(db_emb):
    idx = np.arange(db_emb.shape[0])
    f = faiss.IndexFlatL2(512)
    index = faiss.IndexIDMap(f)
    index.add_with_ids(db_emb, idx)
    with open('model/db_faiss.bin', 'wb'):
        faiss.write_index(index, 'model/db_faiss.bin')

def search_faiss(embs, target, threshold):
    query_db = np.vstack([x.astype('float32') for x in embs])
    res = faiss.StandardGpuResources()
    f_cpu = faiss.IndexFlatL2(512)
    f_cpu = faiss.read_index('model/db_faiss.bin')
    dist, idx = f_cpu.search(query_db, 1)
    
    pred = list(idx[:, 0])
    for i in range(len(pred)):
        if dist[:,0][i] > threshold:
            pred[i] = 'Unknown'
        else:
            pred[i] = target[idx[:,0][i]]
    # top 5
    # pred = idx
    # for i in range(pred.shape[0]):
    #     for j in range(pred.shape[1]):
    #         if dist[i][j] > threshold:
    #             pred[i][j] = 'Unknown'
    #         else:
    #             pred[i] = target[idx[i][j]]
    return pred, dist[:,0]

def get_embedding(folder_img, mode='test'):
    trans = transforms.Compose([
                transforms.Resize((112,112)),
                transforms.ToTensor(),
                # transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                    # std=[0.229, 0.224, 0.225]),
                transforms.Normalize([0.5, 0.5, 0.5],
                                     [0.5, 0.5, 0.5])
    ])

    # model = resnet.ResNet_101(input_size=[112,112])
    # model = IR_SE_50(input_size=[112,112])
    # model = IR_SE_152(input_size=[112,112])
    model = iresnet100()
    # model = iresnet50()
    model.load_state_dict(torch.load(args.pretrained, map_location=device))
    # model = InceptionResnetV1(pretrained='vggface2')
    
    model.eval().to(device)
    
    embs = []
    labels = []
    imgs_name = []
    with torch.no_grad():
        for idx in os.listdir(folder_img):
            for img_name in os.listdir(os.path.join(folder_img, idx)):   
                img_path = os.path.join(folder_img, idx, img_name)
                # img = Image.open(img_path)
                # img = trans(img)
                img = cv2.imread(img_path)
                img = cv2.resize(img, (112,112))
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                if mode =='save':
                    for i in range(5):
                        im = augment(img)
                        im = torch.from_numpy(im).permute(2,0,1).unsqueeze(0).float().to(device)
                        im.div_(255).sub_(0.5).div_(0.5)
                        # img = image_standard(img)
                        # img = img.unsqueeze(0).to(device)
                        emb = model(im)
                        emb = emb.detach().cpu().numpy()
                        emb = np.squeeze(emb)
                        embs.append(emb)
                        labels.append(idx)
                        imgs_name.append(img_name)
                else:
                    img = torch.from_numpy(img).permute(2,0,1).unsqueeze(0).float().to(device)
                    img.div_(255).sub_(0.5).div_(0.5)
                    emb = model(img)
                    emb = emb.detach().cpu().numpy()
                    emb = np.squeeze(emb)
                    embs.append(emb)
                    labels.append(idx)
                    imgs_name.append(img_name)

    return imgs_name, embs, labels
    
if __name__ == "__main__":

    parser = argparse.ArgumentParser('Get embedding all images')
    parser.add_argument('--pretrained', type=str, default='arcface/checkpoint/epoch_31_top1_irse_152_acc94.1612.pth', help='path to pretrained model')
    parser.add_argument('--imgs_db', type=str, default='data_train_crop', help='')
    parser.add_argument('--imgs_test', type=str, default='data_test_crop', help='')
    parser.add_argument('--save_dir', type=str, default='data_emb', help='path to save file embedding')
    parser.add_argument('--save_csv', type=str, default='test_submit.csv', help='')
    parser.add_argument('--save', action='store_true')
    parser.add_argument('--threshold', type=float, default=1000, help='')
    args = parser.parse_args()
    if not os.path.exists(args.save_dir):
        os.mkdir(args.save_dir)

    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    if args.save:
        _, embs, labels = get_embedding(args.imgs_db, mode='test')
        save_embs(embs, labels)
        print('Done save!')
    all_embs, all_labels = load_data('data_emb/all_embs_train.npy', 'data_emb/all_label_train.npy')
    save_faiss(all_embs)
    imgs_name, embs, labels = get_embedding(args.imgs_test)
   
    preds, dist = search_faiss(embs, all_labels, args.threshold)
    vf = [imgs_name[i].split('_')[0]==preds[i] for i in range(len(preds))]
    df = pd.DataFrame({'name_img': imgs_name,
                       'predict': preds,
                       'bool': vf, 
                       'distance': dist})

    df.to_csv(args.save_csv, index= True)

    # plot_distance(all_embs, all_labels)
    # classify(all_embs, all_labels)
    
        
    

