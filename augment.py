import imgaug as ia
import imgaug.augmenters as iaa 
import os
from PIL import Image
from imgaug.augmenters.blur import GaussianBlur
from imgaug.augmenters.color import AddToHueAndSaturation
from imgaug.augmenters.flip import Fliplr
import numpy as np
from numpy.lib.twodim_base import fliplr

aug = iaa.Sequential([
    iaa.OneOf([
        iaa.Fliplr(0.5),
        iaa.AddToHueAndSaturation((-20,20)),
        iaa.GaussianBlur((0,2.0)),
        iaa.Sharpen(alpha=(0,0.5), lightness=(0.7,1.3)),
        iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5),
        iaa.pillike.EnhanceSharpness(3.0),
        iaa.pillike.EnhanceColor(2.0),
        iaa.Multiply((0.5, 1.5))
        ])
    ])

img = Image.open('data_train_crop/84325013206/84325013206_sf_01.png')
img.show()
img = img.resize((112,112))
img = np.array(img)
for _ in range(10):
    img_aug = aug(image=img)
    img_aug = Image.fromarray(img_aug)
    img_aug.show()