import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math 

class ArcFace(nn.Module):
    def __init__(self, in_features, out_features, scale= 64.0, margin= 0.5, easy_margin= False, ls_eps=0.0):
        super(ArcFace, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.scale = scale
        self.margin = margin
        self.easy_margin = easy_margin
        self.ls_eps = ls_eps

        self.weight = nn.Parameter(torch.FloatTensor(out_features, in_features)) 
        nn.init.xavier_uniform_(self.weight)

        self.cos_m = math.cos(margin)
        self.sin_m = math.sin(margin)

        self.th = math.cos(math.pi - margin)
        self.mm = math.sin(math.pi - margin) * margin
        
    def forward(self, input, label):
        cosine = F.linear(F.normalize(input), F.normalize(self.weight))
        sine = torch.sqrt(1.0 - torch.pow(cosine, 2))
        phi = cosine * self.cos_m - sine * self.sin_m
        
        if self.easy_margin:
            phi = torch.where(cosine > 0, phi, cosine)
        else:
            phi = torch.where(cosine > self.th, phi, cosine - self.mm)
        
        one_hot = torch.zeros(cosine.size(), device='cuda')
        one_hot.scatter_(1, label.view(-1, 1).long(), 1)
        
        if self.ls_eps > 0:
            one_hot = (1 - self.ls_eps) * one_hot + self.ls_eps / self.out_features

        output = (one_hot * phi) + ((1.0 - one_hot) * cosine)
        output *= self.scale
        
        return output

class SphereFace(nn.Module):
    def __init__(self, in_features, out_features, m=4) -> None:
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.m = m
        self.base = 1000.
        self.gamma = 0.12 
        self.power = 1
        self.lambdamin = 5.0
        self.iter = 0

        self.weight = nn.Parameter(torch.FloatTensor(out_features, in_features))
        nn.init.xavier_normal_(self.weight)

        self.mlambda = [
            lambda x: x ** 0,
            lambda x: x ** 1,
            lambda x: 2 * x ** 2 - 1,
            lambda x: 4 * x ** 3 - 3 * x,
            lambda x: 8 * x ** 4 - 8 * x ** 2 + 1,
            lambda x: 16 * x ** 5 - 20 * x ** 3 + 5 * x
        ]

    def forward(self, input, label):
        self.iter += 1 
        self.lamb = max(self.lambdamin, self.base * (1 + self.gamma * self.iter) ** (-1 * self.power))
        cos_theta = F.linear(F.normalize(input), F.normalize(self.weight))
        cos_theta = cos_theta.clamp(-1,1)
        cos_m_theta = self.mlambda[self.m](cos_theta)
        theta = cos_theta.data.acos()
        k = (self.m * theta / math.pi).floor()
        phi_theta = ((-1.0) ** k) * cos_m_theta - 2 * k
        norm_of_feature = torch.norm(input, 2, 1)
        
        # convert label to one hot
        one_hot = torch.zeros(cos_theta.size(), device='cuda')
        one_hot.scatter_(1, label.view(-1,1), 1)
        
        # compute output
        output = (one_hot * (phi_theta - cos_theta) / (1 + self.lamb)) + cos_theta
        output *= norm_of_feature.view(-1,1)
        return output



